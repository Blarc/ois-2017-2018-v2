window.addEventListener('load', function() {
	//stran nalozena
	var obdelajKlik = function() {
		var ime = document.querySelector("#uporabnisko_ime").value;
		console.log(ime);
		document.getElementById("uporabnik").innerHTML = ime;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	}
	
	var gumb = document.querySelector("#prijavniGumb");
	gumb.addEventListener('click', obdelajKlik);
	
	var dodajOpomnik = function() {
		var naziv_opomnika = document.querySelector("#naziv_opomnika").value;
		var cas_opomnika = document.querySelector("#cas_opomnika").value;
		document.getElementById("naziv_opomnika").value = "";
		document.getElementById("cas_opomnika").value = "";
		var opomniki = document.querySelector("#opomniki");
		
		opomniki.innerHTML += " \
			<div class='opomnik'> \
				<div class='naziv_opomnika'> "+naziv_opomnika+" </div> \
				<div class='cas_opomnika'> Opomnik čez <span>"+cas_opomnika+"</span> sekund. \
			</div> \
				</div>";
	}
	
	var gumb2 = document.querySelector("#dodajGumb");
	gumb2.addEventListener('click', dodajOpomnik);
	
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);

});
